---
pagetitle: "Clase 05: Markdown"
title: "Taller de R Aplicado a la Investigación en Ciencias Económicas y Empresariales"
subtitle: "Clase 05: Markdown"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Facultad de Ciencias Económicas y Empresariales, Universidad del Magdalena #[`r fontawesome::fa('globe')`]()
output: 
  html_document:
    theme: flatly
    highlight: haddock
    # code_folding: show
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    # fig_width: 7 ## Optional: Set default PDF figure width
    # fig_height: 6 ## Optional: Set default PDF figure height
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: darkblue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(knitr,tidyverse,janitor,rio,skimr,kableExtra)
Sys.setlocale("LC_CTYPE", "en_US.UTF-8") # Encoding UTF-8

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!--==============-->
<!--==============-->
## **[1.] creando un Rmarkdown**

<!------------------>
### **1.1 que es rmarkdown**
En términos sencillos RMarkdown es un procesador de texto que ofrece además la posibilidad de incluir trozos de código desde R (u otros formatos). Este permite trabajar en un sólo documento tanto la redacción  de reportes, asimismo la presentación formal de resultados de análisis estadísticos 

<!------------------>
### **1.2 Como crear un documento rmarkdown**

* **Paso 1:**

![](pics/paso 1.png)

* **Paso 2:**

![](pics/paso 2.png)

* **Paso 3:**

![](pics/paso 3.png)

*Warning:* Los `html` se usan solo para compilar paginas web, `pdf/word` se usan para compilar reportes....

<!------------------>
### **1.3 Como compilar un documento rmarkdown**

La compilación(`knit`) de un documento rmarkdown es la manera por la cual se obtiene un ouput. Se compila presionando knit:

![](pics/knit.png)

<!--==============-->
<!--==============-->
## **[2.] Título, subtitulo, negrilla...**

<!------------------>
### **2.1 negrilla y saltos de paginas**

Se resalta un **texto/palabra** con `**Texto**`, y, se salta a la siguiente pagina con `\pagebreak`.

<!------------------>
### **2.2 Headings**

`rmarkdown` contiene una jerarquía implícita para los diferentes títulos:

```
# H1
## H2
### H3
#### H4
##### H5

```
###  **2.3 Ejemplo**
Si se siguen las anteriores instrucciones un documento de rmarkdown puede tener la siguiente herarquia

![](pics/herarquia.png)


<!--==============-->
<!--==============-->
## **[3.] Chunks**

<!----------------->
### **3.1 ¿que es un chunk?**
Chunks le indican a `rmarkdowd` que se desea ejecutar alguna linea de código. 

![](pics/chunk.png)

```{r}
plot(mtcars$hp)
```

<!----------------->
### **3.2 Modificando Chunks**

Los chunks se modifican de la siguiente manera, ejemplo:

#### **Mostrar solo el codigo**
![](pics/codigo.png)

```{r, eval=F}
plot(mtcars$hp)
```

#### **Mostrar el resultado**
![](pics/resultado.png)

```{r, echo=F}
plot(mtcars$hp)
```

#### **No mostrar ninguno de los dos**
![](pics/ninguno.png)
```{r, include=F}
plot(mtcars$hp)
```


<!--==============-->
<!--==============-->
## **[4.] Addons**

<!----------------->
### **4.1 insertar un hiperlink**

Un hiperlink de una pagina web se inserta de la siguiente manera `[texto](link)`. [link de la Unimag](https://www.unimagdalena.edu.co/)

<!----------------->
### **4.2 insertando una foto**

Una foto se inserta de una manera muy similar al hiperlink, la única diferencia es la adición de un signo de exclamacion *!*. `![caption de la foto](link)`

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQF6degdLQS1-SvHdklkLudno_mPdBz8z7fzQ&usqp=CAU)

<!----------------->
### **4.3 insertando funciones en latex**

Funciones latex son agregadas solo con los símbolos `$ texto de latex $`. **Warning:** Si es su primera vez compilando funciones de latex en su equipo, corra la siguiente función: `tinytex::install_tinytex()`.

**Ej:** el promedio de una vector de números se escribe de la siguiente manera: `$$promedio =\frac{X_1 + X_2 ,..., +X_n}{n} $$`

$$promedio =\frac{X_1 + X_2 ,..., +X_n}{n} $$

[Más información sobre formulas de latex](https://wiki.geogebra.org/es/C%C3%B3digo_LaTeX_para_las_f%C3%B3rmulas_m%C3%A1s_comunes)
